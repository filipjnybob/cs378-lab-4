// Copyright Epic Games, Inc. All Rights Reserved.


#include "CS378_Lab4GameModeBase.h"
#include "Lab4Character.h"
#include "Lab4PlayerController.h"

ACS378_Lab4GameModeBase::ACS378_Lab4GameModeBase() {

	PlayerControllerClass = ALab4PlayerController::StaticClass();

	// Set default pawn class to our character class
	static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(TEXT("Class'/Game/Blueprints/Lab4CharacterBP.Lab4CharacterBP_C'"));

	if (pawnBPClass.Object) {
		UClass* pawnBP = (UClass*)pawnBPClass.Object;
		DefaultPawnClass = pawnBP;
	}
	else {
		DefaultPawnClass = ALab4Character::StaticClass();
	}
}